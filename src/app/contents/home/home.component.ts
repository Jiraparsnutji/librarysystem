import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  book: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.listBooks();
    
  }

  listBooks(){
    this.http.get('localhost:8080/bookdetail/listBooks').subscribe((res: Response) => {

      this.book = res;
    });
  }

}
