import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-recently-content',
  templateUrl: './recently-content.component.html',
  styleUrls: ['./recently-content.component.css']
})
export class RecentlyContentComponent implements OnInit {
 
  bookID:any;
  bookdetails:any;
  name: any;
  author: any;
  releaseDate: any;
  category: any;
  order: any;
  book: Response;

  constructor(private route: ActivatedRoute,private http: HttpClient) { }

  ngOnInit() {
    this.route.paramMap.subscribe(() => {
      this.bookID = this.route.snapshot.params.id;
    });
    this.listBooks();
  }

  getBookDet(){

    this.http.get('localhost:8080/bookdetail/getBookDetail' + this.bookID).subscribe((res: Response) => {

      this.bookdetails = res;
      this.name = this.bookdetails.name;
      this.author = this.bookdetails.author;
      this.releaseDate = this.bookdetails.releaseDate;
      this.category = this.bookdetails.category;
      this.order = this.bookdetails.order;

    });

  }
  listBooks(){
    this.http.get('localhost:8080/bookdetail/listBooks').subscribe((res: Response) => {

      this.book = res;
    });
  }

  
}
