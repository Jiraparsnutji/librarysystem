import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { Ng2SmartTableModule } from 'ng2-smart-table';
// import { FileSelectDirective } from 'ng2-file-upload';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './contents/home/home.component';
import { SearchComponent } from './contents/search/search.component';
import { CategoriesComponent } from './shared/components/header/categories/categories.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { ShowContentComponent } from './contents/show-content/show-content.component';
import { TopbarComponent } from './shared/components/header/topbar/topbar.component';
import { AuthInterceptor } from './shared/auth.interceptor';
import { RecentlyContentComponent } from './contents/recently-content/recently-content.component';
import { ImgFallbackModule } from 'ngx-img-fallback';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShowDetailComponent } from './contents/show-detail/show-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    CategoriesComponent,
    FooterComponent,
    ShowContentComponent,
    TopbarComponent,
    RecentlyContentComponent,
    ShowDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ImgFallbackModule,
    NgbModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
