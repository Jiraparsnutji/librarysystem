import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

export class AuthInterceptor implements HttpInterceptor {

  // Source: https://blog.angular-university.io/angular-jwt-authentication/
  // This interceptor catches all outgoing HTTP requests made by HTTPClient and attach the token if the token exists
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // If there is a header 'role': 'admin', don't add any token

    // Retrieve the token from local storage
    const token = localStorage.getItem('token');
    // tslint:disable-next-line:max-line-length
    // If token exists, but not already set from somewhere else (for example, admin page) set the token into the header of this outgoing HTTP request
    if (token && !req.headers.get('token')) {
      // console.log('There is a token: ' + req.headers.get('token'));
      // console.log(`Token is added to this request => ${req.url}`);
      const cloned = req.clone({
        setHeaders: {
          token
        }
      });
      // Continue the HTTP request
      return next.handle(cloned);
    } else {
      // console.log(`No token added to this request => ${req.url}`);
      // Continue the HTTP request without any changes
      return next.handle(req);
    }
  }
}
