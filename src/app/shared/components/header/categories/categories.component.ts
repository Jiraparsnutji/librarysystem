import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  namemenu = 'HOME';
  chkBtn = 0;

  constructor(
    private router: Router) { }

  ngOnInit() {
   
  }

  homeClick() {
    this.chkBtn = 0;
    this.router.navigate(['']);
    this.namemenu = 'HOME';
  }

}
