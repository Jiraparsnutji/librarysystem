import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './contents/home/home.component';
import { RecentlyContentComponent } from './contents/recently-content/recently-content.component';
import { ShowDetailComponent } from './contents/show-detail/show-detail.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'newcontent', component: RecentlyContentComponent},
  { path: 'showdetail', component: ShowDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
